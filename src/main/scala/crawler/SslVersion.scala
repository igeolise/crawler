package crawler

sealed abstract class SslVersion(val name: String)

object SslVersion {
  case object SslV3 extends SslVersion("SSLv3")
  case object TlsV1 extends SslVersion("TLSv1")
  case object TlsV1_1 extends SslVersion("TLSv1.1")
  case object TlsV1_2 extends SslVersion("TLSv1.2")

  val all: Set[SslVersion] = Set(SslV3, TlsV1, TlsV1_1, TlsV1_2)
}
