package crawler.command_parser

object DiscriminatorParser {
  private val regex = "(\\w+)(?: (.+))".r
  def unapply(command: String): Option[Discriminator] = {

    command match {
      case regex(name, value) =>
        name match {
          case "id" => Some(Id(value))
          case "name" => Some(Name(value))
          case "title" => Some(Title(value))
          case "text" => Some(Text(value))
          case "xPath" => Some(XPath(value))
          case "containsText" => Some(ContainsText(value))
          case _ => None
        }
      case _ => None
    }
  }
}