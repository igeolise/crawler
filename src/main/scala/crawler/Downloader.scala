package crawler

import java.net.URI
import javax.net.ssl.SSLContext
import java.security.cert.X509Certificate
import javax.xml.bind.DatatypeConverter

import org.apache.http.client.methods.{HttpGet, HttpHead, HttpRequestBase}
import org.apache.http.conn.ssl.{DefaultHostnameVerifier, NoopHostnameVerifier, SSLConnectionSocketFactory}
import org.apache.http.conn.util.PublicSuffixMatcherLoader
import org.apache.http.cookie.Cookie
import org.apache.http.impl.client.{BasicCookieStore, DefaultRedirectStrategy, HttpClientBuilder}
import org.apache.http.protocol.HttpContext
import org.apache.http.ssl.{SSLContexts, TrustStrategy}
import org.apache.http.{Header, HttpEntity, HttpRequest, HttpResponse}

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 8/16/13
 * Time: 12:34 PM
 * To change this template use File | Settings | File Templates.
 */
class Downloader {
  /**
   * Downloads given url via GET and returns response entity.
   *
   * Import `crawler._` and use `download(url).getContent.getBytes` to get
   * actual content bytes.
   */
  def download(
    url: String,
    cookies: Set[Cookie] = Set.empty,
    credentials: Option[(String, String)] = None,
    ignoreSslHostname: Boolean = false,
    acceptAllCerts: Boolean = false,
    sslVersions: Set[SslVersion] = SslVersion.all
  ): HttpEntity = {

    val builder = HttpClientBuilder.create()
    val cookieStore = new BasicCookieStore()
    cookies.foreach(cookieStore.addCookie)
    builder.setDefaultCookieStore(cookieStore)


    /**
     * This is interesting.
     * Apache's HttpClient sets original headers to the new request after
     * original gets redirected. This is a problem when downloading files from
     * bitbucket.
     *
     * This is wrong in the following situation:
     * 1. We send a request to download a file to bitbucket.com with
     *    'Authorization' header.
     * 2. Bitbucket returns response which is a redirect to
     *    'bbuseruploads.s3.amazonaws.com/.../?Signature=UjARZb9...'
     * 3. If we access new URL with 'Authorization' headers, we get an error
     *    from 'bbuseruploads.s3.amazonaws.com' saying that only one of auth
     *    methods (Authorization header, or Signature query param) can be
     *    specified.
     *
     * Header copying logic is in DefaultRequestDirector.handleResponse()
     * method line 1035. We can't really redefine DefaultRequestDirector
     * because that method has too much code.
     *
     * So I chose to redefine DefaultRedirectStrategy.getRedirect method
     * which returns a redirect object which rejects 'Authorization' headers
     * to be set by anyone.
     */
    val redirectStrategy = new DefaultRedirectStrategy() {
      override def getRedirect
      (request: HttpRequest, response: HttpResponse, context: HttpContext): HttpRequestBase = {
        val uri: URI = getLocationURI(request, response, context)
        val method: String = request.getRequestLine.getMethod
        if (method.equalsIgnoreCase(HttpHead.METHOD_NAME)) {
          new HttpHead(uri) {
            override def setHeaders(headers: Array[Header]) {
              super.setHeaders(headers.filterNot(_.getName == "Authorization"))
            }
          }
        }
        else {
          new HttpGet(uri) {
            override def setHeaders(headers: Array[Header]) {
              super.setHeaders(headers.filterNot(_.getName == "Authorization"))
            }
          }
        }
      }
    }

    builder.setRedirectStrategy(redirectStrategy)

    val hostnameVerifier =
      if(ignoreSslHostname) new NoopHostnameVerifier()
      else new DefaultHostnameVerifier(PublicSuffixMatcherLoader.getDefault)

    val supportedProtocols = sslVersions.map(_.name).toArray

    // XXX: null means default
    val supportedCiphers = null

    builder.setSSLSocketFactory(
      new SSLConnectionSocketFactory(
        SSLContext.getDefault,
        supportedProtocols,
        supportedCiphers,
        hostnameVerifier
      )
    )

    if (acceptAllCerts) {
      val trustStrategy = new TrustStrategy() {
        override def isTrusted(chain: Array[X509Certificate], authType: String): Boolean = true
      }
      builder.setSSLContext(
        SSLContexts.custom().loadTrustMaterial(trustStrategy).build()
      )
    }

    val http = builder.build()

    val request = new HttpGet(url)

    credentials.foreach { case (username, password) =>
      val auth =
        DatatypeConverter.printBase64Binary(s"$username:$password".getBytes())
      request.setHeader("Authorization", "Basic " + auth)
      request.getAllHeaders
    }

    val response = http.execute(request)
    response.getEntity
  }
}

object Downloader extends Downloader



