package crawler

import java.io.InputStream
import java.net.URL

import com.gargoylesoftware.htmlunit.{DefaultCredentialsProvider, Page, WebClient}
import com.gargoylesoftware.htmlunit.html.{HtmlElement => HElement, _}
import crawler.command_parser._

import scala.collection.JavaConversions._
import scala.util.Try

case class CrawlerResult(nodes: Set[DomNode], streams: Set[DownloadStream])
case class DownloadStream(url: URL, stream: InputStream)

class InterpreterException(msg: String) extends Exception(msg)

class CommandInterpreter(client: WebClient) {
  import CommandInterpreter._

  def crawl(commands: Seq[Action]): Try[CrawlerResult] = Try {
    commands.headOption match {
      case Some(NavigateTo(url)) =>
        val firstPage = client.getPage[HtmlPage](url)
        val initialData = CommandInterpreterIntermediateResult(newPageStack(firstPage), Set.empty, Set.empty)
        val results = commands.tail.foldLeft(initialData) { case (result, action) => getStackTop(result).transform(result, action) }
        CrawlerResult(results.resultNodes, results.resultStreams)
      case Some(NavigateToDownload(url, credentials)) =>
        credentials.foreach { case (uName, passwd) =>
          val credProvider = new DefaultCredentialsProvider()
          credProvider.addCredentials(uName, passwd)
        }
        val page = client.getPage[Page](url)
        CrawlerResult(Set.empty, Set(DownloadStream(new URL(url), page.getWebResponse.getContentAsStream)))
      case Some(_) => throw new InterpreterException("First command does not navigate to a page!")
      case None => throw new InterpreterException("Empty command list")
    }
  }

  private def executeAction(
    intermediateResult: CommandInterpreterIntermediateResult,
    action: Action
  ): CommandInterpreterIntermediateResult =
      (intermediateResult, action) match {
      case (result, OnCurrentPage) => result.copy(elementStack = getStackTop(result).map(_.getHtmlPage) +: result.elementStack )
      case (result, NavigateTo(newUrl)) =>
        val page = client.getPage[HtmlPage](newUrl)
        result.copy(elementStack = newPageStack(page))
      case (result, In(element)) =>
        val resolvedStackElem = getStackTop(result).map(n =>
          getElements(n, element).headOption.fold {
            throw new InterpreterException(s"Could not resolve element $element")
          }(identity)
        )
        result.copy(elementStack = resolvedStackElem +: result.elementStack)
      case (result, GetParent) =>
        val resolvedNode = result.elementStack.headOption
          .map(_.map(_.getParentNode))
          .fold {
            throw new InterpreterException(s"Could not resolve element parent element")
          }(identity)
        result.copy(elementStack = resolvedNode +: result.elementStack)
      case (result, From(element)) => result.copy(resultNodes = result.resultNodes ++ getStackTop(result).flatMap(n => getElements(n, element)).get
      )
      case (result, TypeIn(text)) =>
        getStackTop(result).forEach(_.asHtmlElement.`type`(text))
        result
      case (result, Click) =>
        val page = getStackTop(result).map(_.asHtmlElement.click[Page].asHtmlPage)
        result.copy(elementStack = page +: result.elementStack)
      case (result, MouseOver) =>
        val page = getStackTop(result).map(_.asHtmlElement.mouseOver().asHtmlPage)
        result.copy(elementStack = page +: result.elementStack)
      case (result, Up) => result.copy(elementStack = result.elementStack.tail)
      case (result, FindContainingInLastResult(element, text)) =>
        val elements = getStackTop(result).flatMap(e => getElements(e, element).filter(_.asXml().toLowerCase().contains(text.toLowerCase))).get
        result.copy(resultNodes = result.resultNodes ++ elements)
      case (result, ClickDownload) =>
        val resultStreams = getStackTop(result).get.map { e =>
          val page = e.asHtmlElement.click[Page]()
          val url = page.getWebResponse.getWebRequest.getUrl
          DownloadStream(url, page.getWebResponse.getContentAsStream)
        }
        result.copy(resultStreams = result.resultStreams ++ resultStreams)
      case (_, a) => throw new InterpreterException(s"bad Command: ${a.toString}")
  }

  private def getStackTop(interpreterIntermediateResult: CommandInterpreterIntermediateResult): StackElement = {
    val top = interpreterIntermediateResult.elementStack.headOption.
      fold(throw new InterpreterException("Node stack empty! This should not have happened!"))(identity)
    top
  }

  private def newPageStack(page: HtmlPage): List[StackElement] = {
    List(SingleNode(page.getDocumentElement))
  }

  def getElements(node: DomNode, element: HtmlElement): List[HElement] = {
    val discriminatorXPath = element.discriminator.fold("")(xPathByDiscriminator)
    node.getByXPath(elementXPath(element) + discriminatorXPath).toList.collect { case e: HElement => e }
  }

  private def xPathByDiscriminator(discriminator: Discriminator): String = {
    discriminator match {
      case XPath(value) => value
      case Id(value) => s"[@id='$value']"
      case Name(value) => s"[@name='$value']"
      case Title(value) => s"[@title='$value']"
      case Text(value) => s"[text()='$value']"
      case ContainsText(value) => s"[contains(text(), '$value')]"
    }
  }

  private def elementXPath(element: HtmlElement): String = {
    val elementName = element match {
      case Form(_) => "form"
      case Input(_) => "input"
      case Anchor(_) => "a"
      case Div(_) => "div"
      case Span(_) => "span"
      case TableRow(_) => "tr"
      case TableDataCell(_) => "td"
      case Label(_) => "label"
      case AnyElement(_) => "*"
      case Paragraph(_) => "p"
    }
    s".//$elementName"
  }

  sealed trait StackElement {
    def transform(
      intermediateResult: CommandInterpreterIntermediateResult,
      action: Action
    ): CommandInterpreterIntermediateResult
    def map[A <: DomNode](op: DomNode => A): StackElement
    def forEach(op: DomNode => Any): StackElement = this.map { e =>
      op(e)
      e
    }
    def flatMap(op: DomNode => Traversable[DomNode]): StackElement
    def get: List[DomNode]
  }

  case class SingleNode(domNode: DomNode) extends StackElement {
    override def transform(
      intermediateResult: CommandInterpreterIntermediateResult,
      action: Action
    ): CommandInterpreterIntermediateResult = {
      action match {
        case ForAllElems(elem) =>
          val newStackElem = MultipleNodes(getElements(domNode, elem))
          intermediateResult.copy(elementStack = newStackElem +: intermediateResult.elementStack)
        case _ => executeAction(intermediateResult, action)
      }
    }
    def map[A <: DomNode](op: DomNode => A): StackElement = this.copy(op(domNode))
    def flatMap(op: DomNode => Traversable[DomNode]) = MultipleNodes(op(domNode).toList)
    def get = List(domNode)
  }

  case class MultipleNodes(nodes: List[DomNode]) extends StackElement {
    def transform(
      intermediateResult: CommandInterpreterIntermediateResult,
      action: Action
    ): CommandInterpreterIntermediateResult = {
      action match {
        case ForAllElems(elem) =>
          val newElems = nodes.flatMap(e => getElements(e, elem))
          val newStackElem = MultipleNodes(newElems)
          intermediateResult.copy(elementStack = newStackElem +: intermediateResult.elementStack)
        case _ => executeAction(intermediateResult, action)
      }
    }
    def map[A <: DomNode](op: DomNode => A): StackElement = this.copy(nodes.map(op))
    def flatMap(op: DomNode => Traversable[DomNode]) = this.copy(nodes.flatMap(op))
    def get = nodes
  }

  case class CommandInterpreterIntermediateResult(
    elementStack: Seq[StackElement],
    resultNodes: Set[DomNode],
    resultStreams: Set[DownloadStream]
  )
}

object CommandInterpreter {
  implicit class DomNodeExt(val node: DomNode) extends AnyVal {
    def asHtmlElement: HElement = node match {
      case e: HElement => e
      case _ => throw new InterpreterException(s"The node ${node.asText()} was not a HTML element")
    }
    def asHtmlPage: HtmlPage = node match {
      case e: HtmlPage => e
      case _ => throw new InterpreterException(s"The node ${node.asText()} was not a HTML page")
    }
    def getHtmlPage: HtmlPage = {
      val page = node.getHtmlPageOrNull
      if (page != null) page else throw new InterpreterException(s"Could not get nodes HTML page")
    }
  }

  implicit class PageExt[A <: Page](val page: A) extends AnyVal {
    def asHtmlPage: HtmlPage = {
      page match {
        case e: HtmlPage => e
        case _ => throw new InterpreterException(s"Page ${page.toString} in not a HTML page")
      }
    }
  }

}
