package crawler.command_parser

import crawler.InterpreterException
import org.specs2.mutable.Specification
import ScriptParser.ParsingException

class ScriptParserSpec extends Specification {
  "scriptParser" >> {
    "must fail on empty string" >> {
      ScriptParser.parse("") must beLeft.like { case e: ParsingException => e.getMessage == "Script is empty" }
    }

    "must fail on string with whitespace" >> {
      ScriptParser.parse(" ") must beLeft.like { case e: ParsingException => true }
    }

    "must fail on string with on invalid string" >> {
      val actionString = "unparsable action"
      ScriptParser.parse(actionString) must beLeft.like { case e: ParsingException => e.getMessage == s"$actionString is not a valid action" }
    }

    "must correctly parse at least one action" >> {
      ScriptParser.parse("onCurrentPage") must beRight(List(OnCurrentPage))
    }

    "must correctly parse multiple actions" >> {
      ScriptParser.parse("onCurrentPage\\click") must beRight(List(OnCurrentPage, Click))
    }

    "must correctly parse an action with an element and discriminator" >> {
      val id = "someId"
      ScriptParser.parse(s"in div having id $id") must beRight(List(In(Div(Some(Id(id))))))
    }
  }
}
