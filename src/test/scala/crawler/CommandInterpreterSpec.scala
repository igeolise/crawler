package crawler

import com.gargoylesoftware.htmlunit.WebClient
import org.specs2.matcher.ValueChecks
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

class CommandInterpreterSpec extends Specification with Mockito with ValueChecks {
  "commandInterpreter" >> {
    def webClientMock = mock[WebClient]

    "must return an exception on empty action list" >> {
      new CommandInterpreter(webClientMock).crawl(Seq.empty) must beFailedTry.withThrowable[InterpreterException]("Empty command list")
    }
  }
}
