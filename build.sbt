organization := "com.igeolise"

name := "crawler"

version := "0.7.0"

scalaVersion := "2.12.7"

crossScalaVersions := Seq("2.11.12", "2.12.7")

scalacOptions in Test ++= Seq("-Yrangepos")

libraryDependencies ++= Seq (
    "net.sourceforge.htmlunit" % "htmlunit" % "2.23"
  , "commons-net" % "commons-net" % "3.3"
  , "org.specs2" %% "specs2-core" % "3.8.9" % "test"
  , "org.specs2" %% "specs2-mock" % "3.8.9" % "test"
)

igeoliseResolverSettings
